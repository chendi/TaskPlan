package com.gem.taskplan.javaBean;

public class Message {

	public String[] column = {"message_id","message_time","message_receive_user",
			"message_send_user","message_content","message_type","message_state" };

	public String tableName = "messages";
	
	private int message_id ;
	private String message_time ;
	private String message_receive_user; 
	private String message_send_user ;
	private String message_content ;
	private String message_type ;
	private String message_state ;
	
	
	public String getMessage_state() {
		return message_state;
	}
	public void setMessage_state(String message_state) {
		this.message_state = message_state;
	}
	public String getMessage_receive_user() {
		return message_receive_user;
	}
	public void setMessage_receive_user(String message_receive_user) {
		this.message_receive_user = message_receive_user;
	}
	public String getMessage_send_user() {
		return message_send_user;
	}
	public void setMessage_send_user(String message_send_user) {
		this.message_send_user = message_send_user;
	}
	public int getMessage_id() {
		return message_id;
	}
	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}
	public String getMessage_time() {
		return message_time;
	}
	public void setMessage_time(String message_time) {
		this.message_time = message_time;
	}
	
	public String getMessage_content() {
		return message_content;
	}
	public void setMessage_content(String message_content) {
		this.message_content = message_content;
	}
	public String getMessage_type() {
		return message_type;
	}
	public void setMessage_type(String message_type) {
		this.message_type = message_type;
	}
	
}
