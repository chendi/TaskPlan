package com.gem.taskplan.javaBean;

import android.net.Uri;

public class WorkNewsBean {
	private String name;
	private String date;
	private String text;
	private String head;
	private Uri uri;
	private boolean isComMM;//是否是自己发送的消息
	public WorkNewsBean() {
		super();
	}
	public WorkNewsBean(String name, String date, String text, boolean isComMM,Uri uri) {
		super();
		this.name = name;
		this.date = date;
		this.text = text;
		this.uri = uri;
		this.isComMM = isComMM;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isComMM() {
		return isComMM;
	}
	public void setComMM(boolean isComMM) {
		this.isComMM = isComMM;
	}
	public boolean getComMM() {
		return isComMM;
	}
	public Uri getUri() {
		return uri;
	}
	public void setUserhead(Uri uri) {
		this.uri = uri;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	
}
