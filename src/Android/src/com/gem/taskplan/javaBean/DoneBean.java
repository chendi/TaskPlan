package com.gem.taskplan.javaBean;

public class DoneBean {

	public String key[] = {"content","member","task_progress","task_message","task_attachment","endtime","zanNumber"};
	
	private String content;
	private String member;
	private String task_progress;
	private String task_message;
	private String task_attachment;
	private String endtime;
	private int zanNumber;
	
	
	
	public int getZanNumber() {
		return zanNumber;
	}
	public void setZanNumber(int zanNumber) {
		this.zanNumber = zanNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getMember() {
		return member;
	}
	public void setMember(String member) {
		this.member = member;
	}
	public String getTask_progress() {
		return task_progress;
	}
	public void setTask_progress(String task_progress) {
		this.task_progress = task_progress;
	}
	public String getTask_message() {
		return task_message;
	}
	public void setTask_message(String task_message) {
		this.task_message = task_message;
	}
	public String getTask_attachment() {
		return task_attachment;
	}
	public void setTask_attachment(String task_attachment) {
		this.task_attachment = task_attachment;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	
}
