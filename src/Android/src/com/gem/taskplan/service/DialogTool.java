package com.gem.taskplan.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalHttp;

import com.gem.taskplan.activity.R;
import com.gem.taskplan.activity.WorkNewsActivity;
import com.gem.taskplan.activity.WorkTableActivity;
import com.gem.taskplan.util.SysApplication;
import com.gem.taskplan.util.Tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
/*
 * 该类是用来创建一个对话框并返回给调用者
 * view 是该对话框的内容的布局
 * title 是该对话框的标题
 */
public class DialogTool {
	private String curName = null;
	private int[] startTime = {0,0,0};
	private int[] endTime = {0,0,0};
	Map<String, String> map = new HashMap<String, String>();
	List<Map<String, String>> list = new ArrayList<Map<String,String>>();
	private Context context;
	public DialogTool(Context context) {
		super();
		this.context = context;
		curName = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
	}
	/*
	 * 返回Dialog
	 * view布局文件
	 * titleName：Dialog的主题
	 */
	public AlertDialog getDialog(final View view,final String titleName,final PopupWindow popupWindow){
		final AlertDialog dialog = new AlertDialog.Builder(context)
		.setTitle(titleName).setView(view).create();
		Button sureBaseTime = (Button) view.findViewById(R.id.sure);
		Button cancelBaseTime = (Button) view.findViewById(R.id.cancel);
		if (titleName.equals("按成员过滤")) {
			
		}
		sureBaseTime.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				map = sendHttpMap(view, titleName);
				popupWindow.dismiss();
				WorkNewsActivity.refreashList(map);
				dialog.cancel();
			}
		});
		cancelBaseTime.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		/*setPositiveButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				map = sendHttpMap(view, titleName);
				popupWindow.dismiss();
				WorkNewsActivity.refreashList(map);
				
			}
		}).setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		}).create();*/
		return dialog;
	}
	/*
	 * 动态过滤用到的方法，返回的是一个map集合
	 * view布局文件
	 * titleName：Dialog的主题
	 */
	public Map<String, String> getHttpMap(final View view,final String titleName){
		System.out.println(titleName);
		new AlertDialog.Builder(context)
		.setTitle(titleName).setView(view).
		setPositiveButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				map = sendHttpMap(view, titleName);
				WorkNewsActivity.refreashList(map);
			}
		}).setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		}).create().show();
		return map;
		//		return list;
	}
	/*
	 * 弹出时间编辑框，并且把设置的时间存放进TextView容器中
	 * textView:TextView容器
	 * time：时间按钮图片
	 */
	public void  getTimeDialog(final TextView textView,RelativeLayout time){
		//		public void  getTimeDialog(final EditText editText,ImageView time){
		time.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Date date = new Date();
				final int curMonth = date.getMonth();
				final int curDay = date.getDate();
				String curDateString = (String) Tools.getSystemTime().replace("-", "").subSequence(0, 4);
				final int curYear = Integer.parseInt(curDateString);
				new DatePickerDialog(context,
						new DatePickerDialog.OnDateSetListener() {
					@Override  
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						System.out.println("当前时间："+curYear+" "+curMonth+" "+curDay);
						System.out.println("设置的时间："+year+" "+monthOfYear+" "+dayOfMonth);
						//判断设置的时间与当前的时间进行比较
						int[] curDate = new int[]{curYear,curMonth,curDay};
						int[] Date = new int[]{year,monthOfYear,dayOfMonth};
						if (compareTime(curDate, Date)) {
							Toast.makeText(context, "设置时间成功！", Toast.LENGTH_SHORT).show();
							textView.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
						}else {
							textView.setText("");
							Toast.makeText(context, "您不能设置过去的时间！", Toast.LENGTH_SHORT).show();
						}
					}
				},curYear, curMonth, curDay).show();
			}
		});
	}
	
	/*public void  getTimeDialog(final TextView textView,TextView time){
		//		public void  getTimeDialog(final EditText editText,ImageView time){
		time.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Date date = new Date();
				final int curMonth = date.getMonth();
				final int curDay = date.getDate();
				String curDateString = (String) Tools.getSystemTime().replace("-", "").subSequence(0, 4);
				final int curYear = Integer.parseInt(curDateString);
				new DatePickerDialog(context,
						new DatePickerDialog.OnDateSetListener() {
					@Override  
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						System.out.println("当前时间："+curYear+" "+curMonth+" "+curDay);
						System.out.println("设置的时间："+year+" "+monthOfYear+" "+dayOfMonth);
						//判断设置的时间与当前的时间进行比较
						int[] curDate = new int[]{curYear,curMonth,curDay};
						int[] Date = new int[]{year,monthOfYear,dayOfMonth};
						if (compareTime(curDate, Date)) {
							Toast.makeText(context, "设置时间成功！", Toast.LENGTH_SHORT).show();
							textView.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
						}else {
							textView.setText("");
							Toast.makeText(context, "您不能设置过去的时间！", Toast.LENGTH_SHORT).show();
						}
					}
				},curYear, curMonth, curDay).show();
			}
		});
	}*/
	
	public boolean compareTime(int[] curDate,int[] date){
		boolean flag = false;
		if (date[0]>curDate[0]) {
			flag = true;
		}else if (date[0]==curDate[0]&&date[1]>=curDate[1]) {
			if (date[1]==curDate[1]&&date[2]>=curDate[2]) {
				flag = true;
			}else if(date[1]>curDate[1]){
				flag = true;
			}else {
				flag = false;
			}
		}else {
			flag = false;
		}
		return flag;
	}
	public void  getTimeDialog(final TextView textView,RelativeLayout time,final String mark){
		//		public void  getTimeDialog(final EditText editText,ImageView time){
		time.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Date date = new Date();
				final int curMonth = date.getMonth();
				final int curDay = date.getDate();
				String curDateString = (String) Tools.getSystemTime().replace("-", "").subSequence(0, 4);
				final int curYear = Integer.parseInt(curDateString);
				new DatePickerDialog(context,
						new DatePickerDialog.OnDateSetListener() {
					@Override  
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						if (mark.equals("time1")) {
							textView.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
								startTime[0] = year;
								startTime[1] = monthOfYear;
								startTime[2] = dayOfMonth;
								if (endTime[0]>0) {
									if (!(compareTime(startTime, endTime))) {
										textView.setText("");
										Toast.makeText(context, "起始时间不能大于结束时间！", Toast.LENGTH_SHORT).show();
									}
								}else {
									Toast.makeText(context, "您没有设置结束时间！", Toast.LENGTH_SHORT).show();
								}
							
						}else if (mark.equals("time2")) {
							textView.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
								endTime[0] = year;
								endTime[1] = monthOfYear;
								endTime[2] = dayOfMonth;
								if (startTime[0]>0) {
									if (!(compareTime(startTime, endTime))) {
										textView.setText("");
										Toast.makeText(context, "结束时间不能小于起始时间！", Toast.LENGTH_SHORT).show();
									}
								}else {
									Toast.makeText(context, "您没有设置起始时间！", Toast.LENGTH_SHORT).show();
								}
						}
					}
				},curYear, curMonth, curDay).show();
			}
		});
	}
	/*
	 * 返回值是一个集合
	 * view:布局文件
	 * titleName:用来判断是以什么方式过滤（"按时间过滤"、"按操作过滤"）
	 */
	public Map<String, String> sendHttpMap(View view,String titleName){
		//		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		final Map<String, String> map = new HashMap<String, String>();
		if (titleName.equals("按时间过滤")) {
			TextView startTime = (TextView) view.findViewById(R.id.startTime);
			TextView endTime = (TextView) view.findViewById(R.id.endTime);
			String start = startTime.getText().toString();
			String end = endTime.getText().toString();
			map.put("startTime", start);
			map.put("endTime", end);
			map.put("action", "baseTime");
			map.put("curName", curName);
			//			list.add(map);
		}else if (titleName.equals("按成员过滤")) {
			EditText userEdit = (EditText) view.findViewById(R.id.userEdit);
			System.out.println("------------"+userEdit.getText().toString());
			map.put("base_user", userEdit.getText().toString());
			map.put("curName", curName);
			map.put("action", "baseUser");
		}else if (titleName.equals("按项目过滤")) {
			EditText projectEdit = (EditText) view.findViewById(R.id.projectEdit);
			map.put("curName", curName);
			map.put("base_project", projectEdit.getText().toString());
			map.put("action", "baseProject");
		}else{
			System.out.println("请求失败，没有您想要的信息！");
		}


		return map;
	}
	
	public static void existDialog(final Activity activity){
		new AlertDialog.Builder(activity).setTitle("您确定您要退出TaskPlan程序吗？").setIcon(R.drawable.taskplan2)
		.setPositiveButton("退出", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SysApplication.getInstance().exit(activity);
//				Intent intent = new Intent(WorkTableActivity.this, ReceiveMessageService.class);
//				// 停止推送服务
//				stopService(intent);
				//System.exit(0);
			}
		}).setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		}).create().show();
	}
	/*
	 * 返回值是一个集合
	 * view:布局文件
	 * titleName:用来判断是以什么方式过滤（"按成员过滤"、"按项目过滤"）
	 */
	/*public String sendRequest(View view,String titleName){
		String string = null;
		if (titleName.equals("按成员过滤")) {
			EditText userEdit = (EditText) view.findViewById(R.id.userEdit);
			map.put("base_user", userEdit.getText().toString());
			map.put("action", "baseUser");
		}else if (titleName.equals("按项目过滤")) {
			EditText projectEdit = (EditText) view.findViewById(R.id.projectEdit);
			map.put("base_project", projectEdit.getText().toString());
			map.put("action", "baseProject");
		}else{
			System.out.println("请求失败，没有您想要的信息！");
		}

		return string;
	}*/
}
























