package com.gem.taskplan.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProjectJson {

	public static String getProjectJson(String[] array,String[] markArray){
		String json = null;
		JSONObject allData = new JSONObject();
		JSONArray sing = new JSONArray();
		for (int i = 0; i < array.length; i++) {
			JSONObject temp = new JSONObject();
			try {
				temp.put(markArray[i], array[i]);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sing.put(temp);
		}
		try {
			allData.put("projectInfo", sing);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		json = allData.toString();
		return json;
		
	}
}
