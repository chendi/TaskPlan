package com.gem.taskplan.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonPackage {

	public static String getProjectJson(String[] array,String[] markArray){
		String json = null;
		JSONObject allData = new JSONObject();
		JSONArray sing = new JSONArray();
		for (int i = 0; i < array.length; i++) {
			JSONObject temp = new JSONObject();
			try {
				temp.put(markArray[i], array[i]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			sing.put(temp);
		}
		try {
			allData.put("projectInfo", sing);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		json = allData.toString();
		return json;
		
	}
	
	
	public static String getJson(String name,String[] array,String[] markArray){
		String json = null;
		JSONObject allData = new JSONObject();
		JSONArray sing = new JSONArray();
		for (int i = 0; i < array.length; i++) {
			JSONObject temp = new JSONObject();
			try {
				temp.put(markArray[i], array[i]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			sing.put(temp);
		}
		try {
			allData.put(name, sing);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		json = allData.toString();
		return json;
		
	}
	
	
	
	public static String getHttpJson(String[] dataArray,String[] markArray){
		String json = null;
		JSONObject allData = new JSONObject();
		JSONArray sing = new JSONArray();
		for (int i = 0; i < dataArray.length; i++) {
			JSONObject temp = new JSONObject();
			try {
				temp.put(markArray[i], dataArray[i]);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			sing.put(temp);
		}
		try {
			allData.put(markArray[markArray.length-1]+"Info", sing);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		json = allData.toString();
		return json;
		
	}
}
