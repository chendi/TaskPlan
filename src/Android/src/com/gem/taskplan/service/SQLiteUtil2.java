package com.gem.taskplan.service;
import java.util.ArrayList;
import java.util.List;

import com.gem.taskplan.javaBean.Message;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SQLiteUtil2 {
	private SQLiteDatabase db = null;
	private String tableName = "messages";
	private int pageSize = 50;


	public SQLiteUtil2(SQLiteDatabase db) {
		super();
		this.db = db;
	}


	public void insert(Message message){
		String sql = "insert into "+tableName+" (message_time,message_receive_user,message_send_user,message_content,message_type,message_state) values(?,?,?,?,?,?)";
		Object args[] = new Object[]{message.getMessage_time(),message.getMessage_receive_user(),message.getMessage_send_user(),message.getMessage_content(),message.getMessage_type(),message.getMessage_state()};
		db.execSQL(sql, args);
		db.close();
	}

	public List<Message> query(int pageCode){
		List<Message> list = new ArrayList<Message>();
		String sql = "select * from messages order by message_id desc limit ?,?";
		System.out.println("333333333");
		String[] value = {String.valueOf((pageCode-1)*pageSize),String.valueOf(pageSize)};
		Cursor cursor = db.rawQuery(sql, value);
		//System.out.println("4444444444");
		for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
			Message message = new Message();
			message.setMessage_id(cursor.getInt(0));
			message.setMessage_time(cursor.getString(1));
			message.setMessage_receive_user(cursor.getString(2));
			message.setMessage_send_user(cursor.getString(3));
			message.setMessage_content(cursor.getString(4));
			message.setMessage_type(cursor.getString(5));
			message.setMessage_state(cursor.getString(6));
			list.add(message);
		}
		db.close();
		return list;
	}
	public boolean update(String time,String content,String state){
		boolean flag =false;
//		String sql = "update messages set message_state = ? where message_time = ?  and message_content=?";
		String sql = "update messages set message_state = '"+state+"' where message_time = '"+time+ "' and message_send_user='"+content+"'";
		System.out.println("SQLiteUtil2类-----"+sql);
//		Object args[] = new Object[]{state,time,content};
//		String args[] = new String[]{state,time,content};
		db.execSQL(sql);
//		Cursor cursor = db.rawQuery(sql, null);
		
	/*	if(!cursor.isAfterLast()){
			System.out.println("更新结果：RRRRRRRRRRRRRRRR结果集室是内容的+++state = "+cursor.getString(6));
			flag =true;
		}else {
			System.out.println("更新结果：RRRRRRRRRRRRRRRR结果集为空");
		}*/
		db.close();
		return flag;
	}
	
	public boolean delete(String name){
		boolean flag = false;
		String sql = "delete from users where name = ?";
		String args[] = new String[]{name};
		Cursor cursor = db.rawQuery(sql, args);
		if(!cursor.isAfterLast()){
			flag =true;
		}
		db.close();
		return flag;
	}
	
	/*public boolean check(String name){
		boolean flag =false;
		String sql = "select * from users where name = ?";
		String args[] = new String[]{name};
		Cursor cursor = db.rawQuery(sql, args);
		if(!cursor.isAfterLast()){
			flag =true;
		}
		db.close();
		return flag;
	}
	public boolean delete(String name){
		boolean flag = false;
		String sql = "delete from users where name = ?";
		String args[] = new String[]{name};
		Cursor cursor = db.rawQuery(sql, args);
		if(!cursor.isAfterLast()){
			flag =true;
		}
		db.close();
		return flag;
	}*/

}
