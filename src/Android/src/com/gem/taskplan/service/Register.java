package com.gem.taskplan.service;

import com.gem.taskplan.activity.TabHostActivity;
import com.gem.taskplan.activity.WorkTableActivity;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

/*
 * 登陆 和 注册 时提交数据到服务器的逻辑服务类
 * uname是用户名，upass是用户密码
 * action是请求的动作类型（ 登陆 或者 注册）
 */

public class Register {

	//	private String requestUrl = "http://10.204.1.1:8080/taskPlan/RegisterServlet";
	private String curUser;
	private String requestUrl;
	private SharedPreferences sharedPreferences ;
	private Context context;
	private MySQLiteOpen open;
	private SQLiteUtil util;
	public Register(Context context,String url){
		this.context = context;
		this.requestUrl = url;
		this.sharedPreferences = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE);
		this.curUser = sharedPreferences.getString("lastUser", "");
	}

	//提交请求，向服务器传递数据
	public void submitRequest(final String uname,final String upass,final String action) {

		/*********验证是否连接网络*************/
		if(!CheckedNetWork.checkedConnection(context)){
			return;
		}
		
		AjaxParams params = new AjaxParams();
		params.put("action", action);
		params.put("username", uname);
		params.put("password", upass);

		FinalHttp fh = new FinalHttp();
		System.out.println("@@@@@@@@@访问地址："+requestUrl);
		fh.post(requestUrl, params, new AjaxCallBack<String>(){
			@Override
			public void onLoading(long count, long current) {
				System.out.println("onLoading执行完毕！@！！");
			}

			@Override
			public void onSuccess(String t) {//注册成功
				
				open = new MySQLiteOpen(context, "user.db", 2);
				util = new SQLiteUtil(open.getReadableDatabase());
				boolean flag = util.check(uname);
				util = new SQLiteUtil(open.getWritableDatabase());
				if(!flag){
					System.out.println("查询结果："+flag);
					util.insert(uname, upass);
				}
				
				System.out.println("服务器回写的值是："+t.toString());
				if(action.equals("register")&&t.equals("true")){

					Toast.makeText(context, "恭喜您，注册成功！", Toast.LENGTH_SHORT).show();
					//注册成功后跳转到登陆界面
					Intent intent = new Intent();
					intent.putExtra("userName", uname);
					intent.putExtra("userPass", upass);
					((Activity) context).setResult(Activity.RESULT_OK, intent);
					((Activity) context).finish();

				}else if (action.equals("login")&&t.equals("true")){
					//Toast.makeText(context, "登陆成功！", Toast.LENGTH_SHORT).show();
					/*************启动Service接收服务器端发送的消息***********************/
					int temp =sharedPreferences.getInt(curUser+"PushState", 0);
					if(temp==0){
						Intent serviceIntent = new Intent(context, ReceiveMessageService.class);
						// 启动服务
						context.startService(serviceIntent);
						Toast.makeText(context, "后台消息推送已启动！", Toast.LENGTH_SHORT).show();
						System.out.println("serviceserviceserviceserviceserviceserviceserviceserviceserviceservice");
					}else {
						Toast.makeText(context, "后台消息推送已关闭！", Toast.LENGTH_SHORT).show();
					}

					/********************跳转到工作台界面***************************/
					Intent intent = new Intent(context,TabHostActivity.class);
					intent.putExtra("user_name", uname);
					context.startActivity(intent);
					System.out.println("已跳转到workTableActivity");

				}else if(t.equals("false")){
					Toast.makeText(context, "该用户不存在！", Toast.LENGTH_SHORT).show();
				}else if(t.equals("false2")){
					Toast.makeText(context, "该账号已存在，请更换账号名！", Toast.LENGTH_SHORT).show();
				}
			}  
			@Override  
			public void onFailure(Throwable t, String strMsg) {
				System.out.println("失败了："+strMsg);
				if(action.equals("register")){
					Toast.makeText(context, "注册失败！！", Toast.LENGTH_SHORT).show();
				}else if (action.equals("login")){
					Toast.makeText(context, "登陆失败！", Toast.LENGTH_SHORT).show();
				}
				super.onFailure(t, strMsg);
			}
		});
	}

	//注册时对文本内容为空时的校验
	public boolean checkOut(String uname,String upass) {
		if (uname.trim().length()==0) {
			// 用户输入用户名为空
			Toast.makeText(context, "用户名不能为空!", Toast.LENGTH_SHORT).show();
			return false;
		} else if (upass.trim().length()==0) {
			// 密码不能为空
			Toast.makeText(context, "密码不能为空!", Toast.LENGTH_SHORT).show();
			return false;
		}else {
			return true;
		}
	}
}
