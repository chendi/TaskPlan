package com.gem.taskplan.service;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class CheckedNetWork {

	public static boolean checkedConnection(final Context context){
		boolean result = false;

		ConnectivityManager manger = (ConnectivityManager)
				context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manger.getActiveNetworkInfo();
		if(info==null){
			Toast.makeText(context, "你的网络还没有连接好哦！",Toast.LENGTH_SHORT).show();
			getLinkDialog(context);
		}else {
			if(manger.getNetworkInfo(1).getState()==NetworkInfo.State.CONNECTED){
				//Toast.makeText(context, "您联接的是Wifi",Toast.LENGTH_SHORT).show();
				result=true;
			}else {
				//判断是否连接wifi   
				getLinkDialog(context);
			}
			/*if(manger.getNetworkInfo(0).getState()==NetworkInfo.State.CONNECTED){
				Toast.makeText(context, "您联接的是GPRS",Toast.LENGTH_SHORT).show();
			}*/
		}
		return result;
//		return true;
	}
	
	
	private static void getLinkDialog(final Context context){
		//判断是否连接wifi   
		new AlertDialog.Builder(context).setTitle("您没打开wifi,是否打开？")
		.setPositiveButton("是", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent();

				intent.setComponent(new ComponentName("com.android.settings",
						"com.android.settings.wifi.WifiPickerActivity"));
				((Activity) context).startActivityForResult(intent, 100);
			}
		}).setNegativeButton("否", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				   
			}
		}).create().show();
	}
}
