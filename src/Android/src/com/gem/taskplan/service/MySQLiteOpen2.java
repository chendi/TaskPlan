package com.gem.taskplan.service;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteOpen2 extends SQLiteOpenHelper {

	private String tableName = "messages";
	public MySQLiteOpen2(Context context, String name,int version) {
		super(context, name, null, version);
	}

	

	@Override
	public void onCreate(SQLiteDatabase db) {
//		{"message_id","message_time","message_receive_user",
//			"message_send_user","message_content","message_type","message_state" };
		String sql ="create table "+tableName+" (message_id Integer primary key , message_time varchar(20) not null, message_receive_user varchar(20) not null, message_send_user varchar(20) not null,message_content varchar(100),message_type varchar(100),message_state varchar(30))";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql= "drop table if exists " + tableName;
		db.execSQL(sql);
		this.onCreate(db);
	}

}
