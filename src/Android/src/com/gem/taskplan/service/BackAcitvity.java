package com.gem.taskplan.service;

import com.gem.taskplan.activity.R;

import android.app.Activity;
import android.view.View;

public class BackAcitvity {

	public static void back(View view,final Activity activity){
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.finish();
				activity.overridePendingTransition(R.layout.in_from_left, R.layout.out_to_right);
			}
		});
	}
}
