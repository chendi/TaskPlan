package com.gem.taskplan.service;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteOpen extends SQLiteOpenHelper {

	private String tableName = "users";
	public MySQLiteOpen(Context context, String name,int version) {
		super(context, name, null, version);
	}

	

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql ="create table "+tableName+" (id Integer primary key , name varchar(20) not null, pass varchar(20) not null)";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql= "drop table if exists " + tableName;
		db.execSQL(sql);
		this.onCreate(db);
	}

}
