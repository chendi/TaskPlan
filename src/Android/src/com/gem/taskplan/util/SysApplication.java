package com.gem.taskplan.util;
import java.util.ArrayList;
import java.util.LinkedList; 
import java.util.List; 
import java.util.Set;
import java.util.TreeSet;

import com.gem.taskplan.activity.WorkTableActivity;
import com.gem.taskplan.service.ReceiveMessageService;

import android.app.Activity; 
import android.app.Application; 
import android.content.Intent;
import android.widget.Toast;

public class SysApplication extends Application { 
	private List<Activity> mList = new ArrayList<Activity>(); 
	//	private Set<Activity> mList = new TreeSet<Activity>(); 
	private static SysApplication instance; 

	private SysApplication() {   
	} 
	public synchronized static SysApplication getInstance() { 
		if (null == instance) { 
			instance = new SysApplication(); 
		} 
		return instance; 
	} 
	// add Activity  
	public void addActivity(Activity activity) { 
		mList.add(activity);
	} 

	public void exit() { 
		try { 
			for (Activity activity : mList) { 
				if (activity != null){
					activity.finish(); 
				}
			} 
		} catch (Exception e) { 
			e.printStackTrace(); 
		} finally { 
			System.exit(0); 
		}   
	}   


	public void exit(Activity curActivity) { 
		Intent intent = new Intent(curActivity, ReceiveMessageService.class);
		// 停止推送服务
		curActivity.stopService(intent);
		try { 
			for (Activity activity : mList) { 
				if (activity != null){
					activity.finish(); 
				}
			} 
		} catch (Exception e) { 
			e.printStackTrace(); 
		} finally { 
			System.exit(0); 
		}   
	}   
	public void onLowMemory() { 
		super.onLowMemory();     
		System.gc(); 
	}  
}