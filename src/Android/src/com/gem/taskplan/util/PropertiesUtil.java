package com.gem.taskplan.util;
import java.io.InputStream;
import java.util.Properties;
import android.content.Context;

public class PropertiesUtil {

	private static Properties urlProps;
	
	public static Properties getProperties(Context c){
		Properties props = new Properties();
		try {
			//方法一：通过activity中的context攻取setting.properties的FileInputStream
			InputStream in = c.getAssets().open("taskPalnConfig.properties");
			//方法二：通过class获取setting.properties的FileInputStream
			//InputStream in = PropertiesUtill.class.getResourceAsStream("/assets/  setting.properties ")); 
			props.load(in);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		urlProps = props;
		//System.out.println(urlProps.getProperty("serverURL"));
		return urlProps;
	}
	
	//得到访问服务器的地址
	public static String getServerURL(Context context){
		urlProps = getProperties(context);
		String serverURL = urlProps.getProperty("serverURL");
		return serverURL;
	}

}

