package com.gem.taskplan.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Dialog;
import android.os.Environment;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class Tools {

	public static String getSystemTime(){
		String time;
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		time = format.format(date);
		return time;
	}

	//读取文件
	public static String readFile(String path){
		File file = new File(path);
		String attachment = null;
		try {
			FileInputStream in = new FileInputStream(file);
			byte[] buffer = new byte[in.available()];
			in.read(buffer);
			attachment = new String(buffer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return attachment;
	}

	//写出文件
	public static void writeFile(String content){
		//写出到了sdcard根目录下
		File file = new File(Environment.getExternalStorageDirectory()+File.separator+"123.mp3");
		try {
			OutputStream out = new FileOutputStream(file);
			out.write(content.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void checkHttpResult(String result){
		if (result==null) {
			return;
		}
	}
	/*
	 * 适配弹出框的高度与宽度
	 * dialog:要适配的dialog
	 * m：窗口管理器
	 */
	public static void dialogHightWidth(Dialog dialog,WindowManager m){
		Display d = m.getDefaultDisplay(); //为获取屏幕宽、高
		//	Window window = dialog.getWindow();//获取对话框当前的参数值 
		Window window = dialog.getWindow();//获取对话框当前的参数值 
		WindowManager.LayoutParams lp = window.getAttributes();
		lp.height = (int) (d.getHeight() * 0.87); // 高度设置为屏幕的0.6
		lp.width = (int) (d.getWidth() * 0.9); // 宽度设置为屏幕的0.65
		window.setAttributes(lp);
		dialog.show();

	}
}    
