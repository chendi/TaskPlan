package com.gem.taskplan.activity;

import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.service.Register;
import com.gem.taskplan.util.PropertiesUtil;
import com.gem.taskplan.util.SysApplication;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends FinalActivity {

	@ViewInject(id=R.id.register2,click="registerButton")Button registerButton;
	@ViewInject(id=R.id.back,click="back")Button back;
	@ViewInject(id=R.id.newUname)EditText newName ;
	@ViewInject(id=R.id.newUpass1 )EditText newUpass1 ;
	@ViewInject(id=R.id.newUpass2)EditText newUpass2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
//		System.out.println("dkjlfd");
	}

	//注册按钮的监听事件内容
	public void registerButton(View v){

		//验证时否联网
		if(!CheckedNetWork.checkedConnection(this)){
			return;
		}

		String uname = newName.getText().toString();
		String upass1 = newUpass1.getText().toString();
		String upass2 = newUpass2.getText().toString();

		if(upass1.equals(upass2)){
			String url = PropertiesUtil.getServerURL(this)+"/RegisterServlet";
			Register registers = new Register(this,url);//得到一个注册服务类

			if(registers.checkOut(uname, upass1)){//判断文本是否为空
				System.out.println("开始请求");
				registers.submitRequest(uname, upass1,"register");
				System.out.println("请求服务器结束！！！！");

			}else {
				Toast.makeText(this, "密码前后不一致，请重新输入！", Toast.LENGTH_LONG).show();
			}
		}
	}

	//返回按钮的监听事件内容
	public void back(View v){
		onBackPressed();
	}
}
