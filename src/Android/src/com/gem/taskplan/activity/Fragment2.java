package com.gem.taskplan.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gem.taskplan.adapter.TaskDoingAdapter;
import com.gem.taskplan.javaBean.DoingBean;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("ValidFragment")
public class Fragment2 extends Fragment {

	static String url = "/TaskServlet";
	private static Context context;
	private static String curProjectName;
	private  String curTeamName;
	private  String curUser;
	//private static Bundle data2;
	private static ListView listView;
	private static TaskDoingAdapter MyAdapter2;
	private static List<DoingBean> list = new ArrayList<DoingBean>();
	private SwipeRefreshLayout refreshDoing;

	public Fragment2(Context context1,Bundle data,String curProjectName1,String teamName) {
		context = context1;
		this.curTeamName = teamName;
		//data2 = data;
		Fragment2.curProjectName = curProjectName1;
		this.curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		/************获得访问服务器的地址***************/
		url = "/TaskServlet";
		url = PropertiesUtil.getServerURL(context)+url;
	}  

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		View v = inflater.inflate(R.layout.fragment_pager_list, container, false);
		listView = (ListView) v.findViewById(android.R.id.list);
		refreshDoing = (SwipeRefreshLayout) v.findViewById(R.id.refreshTask);
		//添加刷新颜色变化
		refreshDoing.setColorScheme(android.R.color.holo_red_light, android.R.color.holo_green_light,
				android.R.color.holo_blue_bright, android.R.color.holo_orange_light);
		
		MyAdapter2 = new TaskDoingAdapter(context, list, R.layout.content_list2);
		listView.setAdapter(MyAdapter2);
		initFragment2();
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		/**********初始化doing界面**********/

		/************设置适配器*************/
		MyAdapter2 = new TaskDoingAdapter(context, list, R.layout.content_list2);
		listView.setAdapter(MyAdapter2);
		initFragment2();

		/**********doing界面的ListView条目被点击的点击事件********/
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent,
					View view, int position, long id) {
				/****点击跳转到详情界面******/
				Intent intent = new Intent(context,TaskInfoActivity.class);
				TextView textView = (TextView) view.findViewById(R.id.task_content2);
				TextView task_member1 = (TextView) view.findViewById(R.id.task_member2);
				TextView end_time1 = (TextView) view.findViewById(R.id.end_time2);
				intent.putExtra("taskName", textView.getText().toString());
				intent.putExtra("taskMembers", task_member1.getText().toString());
				intent.putExtra("taskOutTime", end_time1.getText().toString());
				intent.putExtra("ProjectName", curProjectName);
				intent.putExtra("TeamName", curTeamName);
				intent.putExtra("taskType", "在做");
				startActivityForResult(intent, 2);
			}
		});
		/**********doing界面的ListView条目被长按的长按事件********/
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					int pos, long arg3) {

				final TextView text = (TextView) view.findViewById(R.id.task_content2);
				final int position = pos;
				//弹出窗口选择是否删除该任务
				new AlertDialog.Builder(context).setIcon(R.drawable.delete).setTitle("删除任务").setMessage("您确定要删除 “ "+text.getText().toString()+" ” 吗？")
				.setPositiveButton("删除", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						/*********判断是否联网***************/
						if(!CheckedNetWork.checkedConnection(context)){
							return;
						}
						
						Map<String, String> map = new HashMap<String, String>();
						map.put("taskName",text.getText().toString());
						map.put("curName", curUser);
						map.put("curProjectName", curProjectName);
						map.put("action", "deleteTask");
						String result = null;
						try {
							result = HttpUtil.postRequest(url, map);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(result.equals("notCreator")){
							Toast.makeText(context, "您不是该任务的创建者，不能删除该任务！", Toast.LENGTH_SHORT).show();
							return;
						}else if(result.equals("yes")){
							list.remove(position);
							MyAdapter2.notifyDataSetChanged();
							Toast.makeText(context, "已成功删除该任务！", Toast.LENGTH_SHORT).show();
						}else {
//							Toast.makeText(context,"网络异常，请稍后重试！", Toast.LENGTH_SHORT).show();
						}

					}
				}).setNegativeButton("取消",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).create().show();

				return false;
			}
		});

		/************listView的下拉刷新监听**************/
		refreshDoing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				new Handler().postDelayed(new Runnable() {
					public void run() {
						refreshDoing.setRefreshing(false);
						initFragment2();//刷新重新访问一次服务器
					}
				}, 500);
			}
		});
	}

	public static void setData2(Bundle aa ){
		if(aa!=null){
			String content = aa.getString("content");
			String member = aa.getString("member");
			String endtime = aa.getString("endtime");
			String attachment = aa.getString("attachment");
			DoingBean bean = new DoingBean();
			bean.setContent(content);
			bean.setMember(member);
			bean.setEndtime(endtime);
			bean.setTask_attachment(attachment);
			bean.setTask_progress("0/0");
			bean.setTask_message("0");
			list.add(0,bean);
			MyAdapter2.notifyDataSetChanged();
		}
	}

	//从服务器获取任务信息
	public static void initFragment2(){
		if(!CheckedNetWork.checkedConnection((Activity)context)){
			return;
		}
		String curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		AjaxParams params = new AjaxParams();
		params.put("curUser",curUser);
		params.put("curProjectName", curProjectName);
		params.put("action", "queryDoing");
		FinalHttp fh = new FinalHttp();
		fh.post(url, params,new AjaxCallBack<String>() {
			@Override
			public void onFailure(Throwable t, String strMsg) {
				super.onFailure(t, strMsg);
				Toast.makeText(context, "添加任务失败", Toast.LENGTH_SHORT).show();
			}
			@Override
			public void onSuccess(String t) {
				//System.out.println("fragment2中初始化时显示的y是++"+t);
				String[] key = new String[]{"task_name","membersName","subTaskFinishNumber","subTaskNumber","commentsNumber","attchmentNumber","task_outOfDate"};

				System.out.println("Fragment2界面："+t);
				try {
					JSONObject object = new JSONObject(t);
					JSONArray array = object.getJSONArray("taskListJson");
					list.clear();
					for(int i=0 ; i<array.length(); i++){
						JSONObject object1 = array.getJSONObject(i);
						JSONArray array1 = object1.getJSONArray("taskListJson"+i);
						DoingBean bean = new DoingBean();
						bean.setContent(array1.getJSONObject(0).getString(key[0]));
						bean.setMember(array1.getJSONObject(1).getString(key[1]));
						String subTaskFinishNumber = array1.getJSONObject(2).getString(key[2]);
						String subTaskNumber = array1.getJSONObject(3).getString(key[3]);
						bean.setTask_progress(subTaskFinishNumber+"/"+subTaskNumber);
						bean.setTask_message(array1.getJSONObject(4).getString(key[4]));
						bean.setTask_attachment( array1.getJSONObject(5).getString(key[5]));
						bean.setEndtime(array1.getJSONObject(6).getString(key[6]));
						list.add(bean);
					}
					MyAdapter2.notifyDataSetChanged();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}

}

