package com.gem.taskplan.activity;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gem.taskplan.service.MyAdapter;
import com.gem.taskplan.util.Open;
import com.gem.taskplan.util.SysApplication;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SDFileManagerActivity extends ListActivity {
	private List<String> items = null;
	private List<String> paths = null;
	private String rootPath = "/";
	private String curPath = "/";
	private TextView mPath;
	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fileselect);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		mPath = (TextView) findViewById(R.id.mPath);

		Button buttonConfirm = (Button) findViewById(R.id.buttonConfirm);
		buttonConfirm.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent data = new Intent(SDFileManagerActivity.this, TaskActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("file", curPath);
				data.putExtras(bundle);
				setResult(2, data);
				finish();
			}
		});
		Button buttonCancle = (Button) findViewById(R.id.buttonCancle);
		buttonCancle.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				System.out.println("退出文件选择器！！！！");
				finish();
			}
		});
		
		System.out.println("sdcard:*****"+Environment.getExternalStorageState());
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
//			rootPath = Environment.getExternalStorageDirectory().getPath();
			rootPath ="/mnt";
			System.out.println(rootPath);
			}else {
				System.out.println("sdcard未装载");
				Toast.makeText(this, "sdcard未装载", Toast.LENGTH_SHORT).show();
				return;
			}
  
		getFileDir(rootPath);
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		curPath = paths.get(position);
		File file = new File(curPath);

		if (file.isDirectory()) {
			getFileDir(curPath);
		} else {
			mPath.setText(curPath);//如果是上传文件就要执行此句
			//可以打开文件
			//startActivity(Open.openFile(file));
		}
	}

	/*************得到文件目录结构************/
	private void getFileDir(String filePath) {
		mPath.setText(filePath);
		items = new ArrayList<String>();
		paths = new ArrayList<String>();
		File f = new File(filePath);
		File[] files = f.listFiles();

		if (!filePath.equals(rootPath)) {
			items.add("b1");
			paths.add(rootPath);
			items.add("b2");
			paths.add(f.getParent());
		}
		//迭代文件
		for(File file : files){
			if(file.isHidden()||file.getName().equals("secure")){
				continue;
			}
			items.add(file.getName());
			paths.add(file.getPath());
		}

		setListAdapter(new MyAdapter(this, items, paths));
	}
	
}