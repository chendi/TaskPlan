package com.gem.taskplan.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gem.taskplan.adapter.TaskTodoAdapter;
import com.gem.taskplan.javaBean.ToDoBean;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("ValidFragment")
public class Fragment1 extends Fragment{

	private static String url = "/TaskServlet";
	private static  String curProjectName;
	private static TaskTodoAdapter MyAdapter1;
	private static List<ToDoBean> list = new ArrayList<ToDoBean>();
	private static  Context context;
	private static  ListView listView;
	private String curTeam;
	private String curUser;
	private SwipeRefreshLayout refreshTodo;
	//Bundle aa;
	ProgressBar pBar1 ;
	public Fragment1(Context context1,Bundle bundle,String curProjectName1,String curTeam1) {
		context = context1;
		this.curTeam = curTeam1;
		curProjectName = curProjectName1;
		this.curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		/************获得访问服务器的地址***************/
		url = "/TaskServlet";
		url = PropertiesUtil.getServerURL(context)+url;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_pager_list, container,false);
		listView = (ListView) v.findViewById(android.R.id.list);
		refreshTodo = (SwipeRefreshLayout) v.findViewById(R.id.refreshTask);
		//添加刷新颜色变化
		refreshTodo.setColorScheme(android.R.color.holo_red_light, android.R.color.holo_green_light,
				android.R.color.holo_blue_bright, android.R.color.holo_orange_light);
		View view1 = inflater.inflate(R.layout.content_list2, null);
		pBar1 =  (ProgressBar) view1.findViewById(R.id.pbar1);
		pBar1.setMax(100);
		pBar1.setProgress(30);
		
		MyAdapter1 = new TaskTodoAdapter(context, list,R.layout.content_list1);
		listView.setAdapter(MyAdapter1);
		initFragment1();
		
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//simpleAdapter = new SimpleAdapter(getActivity(),list, R.layout.content_list, new String[]{"content","member","task_progress","task_message","task_attachment","endtime"}, new int[]{R.id.task_content,R.id.task_member,R.id.task_progress,R.id.task_message,R.id.task_attachment,R.id.end_time} );
		MyAdapter1 = new TaskTodoAdapter(context, list,R.layout.content_list1);
		listView.setAdapter(MyAdapter1);
		initFragment1();


		/**********Todo界面的ListView条目被点击的点击事件********/
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent,
					View view, int position, long id) {
				System.out.println("任务被点击了");
				/****点击跳转到详情界面******/
				Intent intent = new Intent(context,TaskInfoActivity.class);
				TextView textView = (TextView) view.findViewById(R.id.task_content1);
				TextView task_member1 = (TextView) view.findViewById(R.id.task_member1);
				TextView end_time1 = (TextView) view.findViewById(R.id.end_time1);
				intent.putExtra("taskName", textView.getText().toString());
				intent.putExtra("taskMembers", task_member1.getText().toString());
				intent.putExtra("taskOutTime", end_time1.getText().toString());
				intent.putExtra("ProjectName", curProjectName);
				intent.putExtra("TeamName", curTeam);
				intent.putExtra("taskType", "待做");
				startActivityForResult(intent, 1);
			}
		});

		/**********Todo界面的ListView条目被长按的长按事件********/
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					int pos, long arg3) {
				final TextView text = (TextView) view.findViewById(R.id.task_content1);
				final int position = pos;
				//弹出窗口选择是否删除该任务
				new AlertDialog.Builder(context).setIcon(R.drawable.delete).setTitle("删除任务")
				.setMessage("您确定要删除 “ "+text.getText().toString()+" ” 吗？")
				.setPositiveButton("删除", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						/*********判断是否联网***************/
						if(!CheckedNetWork.checkedConnection(context)){
							return;
						}
						
						Map<String, String> map = new HashMap<String, String>();
						map.put("taskName",text.getText().toString());
						map.put("curName", curUser);
						map.put("curProjectName", curProjectName);
						map.put("action", "deleteTask");
						String result = null;
						try {
							result = HttpUtil.postRequest(url, map);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(result.equals("notCreator")){
							Toast.makeText(context, "您不是该任务的创建者，不能删除该任务！", Toast.LENGTH_SHORT).show();
							return;
						}else if(result.equals("yes")){
							list.remove(position);
							MyAdapter1.notifyDataSetChanged();
							Toast.makeText(context, "已成功删除该任务！", Toast.LENGTH_SHORT).show();
						}else {
//							Toast.makeText(context,"网络异常，请稍后重试！", Toast.LENGTH_SHORT).show();
						}

					}
				}).setNegativeButton("取消",new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).create().show();
				return false;
			}
			
		});

		/************下拉刷新监听**************/
		refreshTodo.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				System.out.println("进入了refreshTodo的刷新！！！！！！！！！！！");
				new Handler().postDelayed(new Runnable() {
					public void run() {
						refreshTodo.setRefreshing(false);
						initFragment1();
					}
				}, 500);
			}
		});
	}

	public static void setData1(Bundle aa ){
		if(aa!=null){
			String content = aa.getString("content");
			String member = aa.getString("member");
			String endtime = aa.getString("endtime");
			String attachment = aa.getString("attachment");
			ToDoBean bean = new ToDoBean();
			bean.setContent(content);
			bean.setMember(member);
			bean.setEndtime(endtime);
			bean.setTask_attachment(attachment);
			bean.setTask_progress("0/0");
			bean.setTask_message("0");
			list.add(0,bean);
			MyAdapter1.notifyDataSetChanged();
		}
	}

	//从服务器获取任务信息
	public static void initFragment1(){
		if(!CheckedNetWork.checkedConnection((Activity)context)){
			return;
		}
		String curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		Map<String, String> map = new HashMap<String, String>();
		map.put("curUser",curUser);
		map.put("curProjectName", curProjectName);
		map.put("action", "queryTodo");
		String result = null;
		try {
			result = HttpUtil.postRequest(url, map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Fragment1界面_________"+result);
		if (result==null) {
			return;
		}
		String[] key = new String[]{"task_name","membersName","subTaskFinishNumber","subTaskNumber","commentsNumber","attchmentNumber","task_outOfDate"};

		try {
			JSONObject object = new JSONObject(result);
			JSONArray array = object.getJSONArray("taskListJson");
			list.clear();
			for(int i=0 ; i<array.length(); i++){
				JSONObject object1 = array.getJSONObject(i);
				JSONArray array1 = object1.getJSONArray("taskListJson"+i);
				ToDoBean bean = new ToDoBean();
				bean.setContent(array1.getJSONObject(0).getString(key[0]));
				bean.setMember(array1.getJSONObject(1).getString(key[1]));
				//						String subTaskFinishNumber = array1.getJSONObject(2).getString(key[2]);
				//						String subTaskNumber = array1.getJSONObject(3).getString(key[3]);
				//						bean.setTask_progress(subTaskFinishNumber+"/"+subTaskNumber);
				bean.setTask_message(array1.getJSONObject(4).getString(key[4]));
				bean.setTask_attachment( array1.getJSONObject(5).getString(key[5]));
				bean.setEndtime(array1.getJSONObject(6).getString(key[6]));
				list.add(bean);
			}
			MyAdapter1.notifyDataSetChanged();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/*String curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		AjaxParams params = new AjaxParams();
		params.put("curUser",curUser);
		params.put("curProjectName", curProjectName);
		params.put("action", "queryTodo");
		FinalHttp fh = new FinalHttp();
		fh.post(url, params,new AjaxCallBack<String>() {
			@Override
			public void onFailure(Throwable t, String strMsg) {
				super.onFailure(t, strMsg);
				Toast.makeText(context, "初始化任务失败", Toast.LENGTH_SHORT).show();
			}
			@Override
			public void onSuccess(String t) {
				//System.out.println("fragment1中初始化时显示的y是++"+t);
				String[] key = new String[]{"task_name","membersName","subTaskFinishNumber","subTaskNumber","commentsNumber","attchmentNumber","task_outOfDate"};

				try {
					JSONObject object = new JSONObject(t);
					JSONArray array = object.getJSONArray("taskListJson");
					list.clear();
					for(int i=0 ; i<array.length(); i++){
						JSONObject object1 = array.getJSONObject(i);
						JSONArray array1 = object1.getJSONArray("taskListJson"+i);
						ToDoBean bean = new ToDoBean();
						bean.setContent(array1.getJSONObject(0).getString(key[0]));
						bean.setMember(array1.getJSONObject(1).getString(key[1]));
						//						String subTaskFinishNumber = array1.getJSONObject(2).getString(key[2]);
						//						String subTaskNumber = array1.getJSONObject(3).getString(key[3]);
						//						bean.setTask_progress(subTaskFinishNumber+"/"+subTaskNumber);
						bean.setTask_message(array1.getJSONObject(4).getString(key[4]));
						bean.setTask_attachment( array1.getJSONObject(5).getString(key[5]));
						bean.setEndtime(array1.getJSONObject(6).getString(key[6]));
						list.add(bean);
						MyAdapter1.notifyDataSetChanged();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});*/
	}
}   

