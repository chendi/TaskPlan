package com.gem.taskplan.activity;
import com.gem.taskplan.util.SysApplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Looper;
import android.widget.Toast;

public class VersionUpgradeActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		final 	ProgressDialog pd = new ProgressDialog(this);
		pd.setTitle("TaskPlan v1.0");
		pd.setMessage("版本更新中....");
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setIndeterminate(false);
		pd.setMax(100);
		pd.show();
		
		new Thread(){
			@SuppressLint("NewApi")
			public void run() {
			 for(int i=0;i<=100; i++)	
				{pd.setProgress(i);
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			 pd.dismiss();
			 Looper.prepare();
			 Toast.makeText(getApplicationContext(), "更新成功", Toast.LENGTH_SHORT).show();
			 finish();
			 Looper.loop();
	}
		}.start();
		
	
	}
	
	}

