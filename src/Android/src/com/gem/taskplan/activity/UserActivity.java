package com.gem.taskplan.activity;

import java.util.HashMap;
import java.util.Map;

import net.tsz.afinal.FinalBitmap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gem.taskplan.service.BackAcitvity;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class UserActivity extends Activity {

	private String url = "/SetServlet";
	private ImageView backImageView = null;
	private TextView userName = null;
	private TextView sexPerson = null;
	private TextView photo = null;
	private TextView registerTime = null;
	private TextView email = null;
	private ImageView userEdit = null;
	private ImageView userHead = null;
	private String curName = null;
	private FinalBitmap fb ;
	private SharedPreferences sharedPreferences;
//	 String[] key = new String[]{"user_picture","user_name","user_sex","user_email","user_phone","user_registerDate"};
	 String[] value = new String[6];
			 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_user);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		/************获得访问服务器的地址***************/
		url = PropertiesUtil.getServerURL(this)+url;
//		System.out.println("工作台访问服务器地址：--------------"+url);
		
		inintView();
		sharedPreferences = UserActivity.this.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE);
		curName = sharedPreferences.getString("lastUser", "");
		inintData();
		userEdit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(UserActivity.this, EditUserActivity.class);
				Bundle bundle = new Bundle();
				bundle.putStringArray("userInfo", value);
				intent.putExtras(bundle);
				startActivityForResult(intent, 1);
			}
		});
		BackAcitvity.back(backImageView, UserActivity.this);
		//userEdit.seyo
	}
	/****************************初始化控件*********************************/
	public void inintView(){
		backImageView = (ImageView)findViewById(R.id.backImage);//返回按钮
		userName = (TextView) this.findViewById(R.id.loginName);//用户名
		userEdit = (ImageView) findViewById(R.id.userEdit);//编辑用户信息按钮
		userHead = (ImageView) findViewById(R.id.userInfoHead);//用户头像
		sexPerson = (TextView) findViewById(R.id.sexPerson);//性别
		photo = (TextView) findViewById(R.id.photo);//手机号
		email = (TextView) findViewById(R.id.email);//用户邮箱
		registerTime = (TextView) findViewById(R.id.registerTime);//注册时间
	}
	/****************************链接服务器查询数据*********************************/
	public void inintData(){
		/***********判断是否连接网络**********************/
		if(!CheckedNetWork.checkedConnection(this)){
			return;
		}
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("curName", curName);
		map.put("action", "queryUserInfo");
		String result = null;
		 try {
			result = HttpUtil.postRequest(url, map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		 System.out.println("个人用户信息，来自UserActivity："+result);
		 String[] key = new String[]{"user_picture","user_name","user_sex","user_email","user_phone","user_registerDate"};
		 try {
			JSONObject jsonObject = new JSONObject(result);
			JSONArray jsonArray = jsonObject.getJSONArray("userListJson");
			fb = FinalBitmap.create(UserActivity.this);
			value[0]=jsonArray.getJSONObject(0).getString(key[0]);
			value[1]=jsonArray.getJSONObject(1).getString(key[1]);
			value[2]=jsonArray.getJSONObject(2).getString(key[2]);
			value[3]=jsonArray.getJSONObject(3).getString(key[3]);
			value[4]=jsonArray.getJSONObject(4).getString(key[4]);
			value[5]=jsonArray.getJSONObject(5).getString(key[5]);
			if (value[0].length()==0) {
				userHead.setImageResource(R.drawable.touxiang);
			}else {
				fb.closeCache();
				fb.closeCache();
				fb.display(userHead, value[0]);
			}
			userName.setText(value[1]);
			sexPerson.setText(value[2]);
			email.setText(value[3]);
			photo.setText(value[4]);
			registerTime.setText(value[5]);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data!=null) {
			SharedPreferences sharedPreferences = getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE);
			Editor editor = sharedPreferences.edit();
			
			curName = data.getStringExtra("newUserName");
			editor.putString("lastUser", curName);
			editor.commit();
		}
		inintData();
	}
}