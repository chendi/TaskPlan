package com.gem.taskplan.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.tsz.afinal.FinalBitmap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gem.taskplan.activity.R.string;
import com.gem.taskplan.adapter.MyTeamAdapter;
import com.gem.taskplan.service.BackAcitvity;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class MyTeamActivity extends Activity {

	private SharedPreferences sharedPreferences;       //得到保存的信息
	private String curUserName;
	private ImageView addTeam  = null;//添加团队按钮
	private ListView myTeamlListView = null;//我的团队的listview
	private List<Map<String, String>> list = null;//用来的listview中存放的map
	private MyTeamAdapter myTeamAdapter = null;//团队列表的listview的适配器
	private LayoutInflater inflater = null;//用来获取控件
	private ImageView back = null;				//返回按钮
	private int REQUEST_CODE = 1;
	private String url = "/SetServlet";
	private SwipeRefreshLayout refreshLayout;
	private FinalBitmap fb ;
	private ImageView userHead;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_team);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		/************获得访问服务器的地址***************/
		url = PropertiesUtil.getServerURL(this)+url;
		
		/****************************************/
		sharedPreferences = MyTeamActivity.this.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE);
		curUserName = sharedPreferences.getString("lastUser", "");
		inintView();//得到控件
		list = new ArrayList<Map<String,String>>();
		myTeamAdapter = new MyTeamAdapter(list, MyTeamActivity.this);
		myTeamlListView.setAdapter(myTeamAdapter);
		refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshWorkTable);
		refreshLayout.setColorScheme(android.R.color.holo_red_light, android.R.color.holo_green_light,
				android.R.color.holo_blue_bright, android.R.color.holo_orange_light);//添加刷新颜色变化
		inintHttp();
		addTeam.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MyTeamActivity.this,AddTeamActivity.class);
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
		myTeamlListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				Intent intent = new Intent(MyTeamActivity.this, AddTeamActivity.class);
				intent.putExtra("teamName", list.get(position).get("team"));
				intent.putExtra("edit", "edit");
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
		myTeamlListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					int pos, long arg3) {
				final int position = pos;
				new AlertDialog.Builder(MyTeamActivity.this).setTitle("您确定要删除此团队吗？")
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						/*********判断是否联网***************/
						if(!CheckedNetWork.checkedConnection(MyTeamActivity.this)){
							return;
						}
						
						String teamName = list.get(position).get("team");
						Map<String, String> map = new HashMap<String, String>();
						map.put("teamName", teamName);
						map.put("action", "deleteTeam");
						String result = null;
						try {
							result = HttpUtil.postRequest(url, map);
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println("*********判断删除团队是否成功****************   "+result);
						if (result.equals("yes")) {
							list.remove(position);
							myTeamAdapter.notifyDataSetChanged();
							Toast.makeText(MyTeamActivity.this, "成功删除团队！", Toast.LENGTH_SHORT).show();
						}else {
//							Toast.makeText(MyTeamActivity.this, "网络不好，团队没有删除成功！", Toast.LENGTH_SHORT);
						}
					}
				}).setNegativeButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				}).create().show();

				return false;
			}
		});
		refreshLayout.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				refreshLayout.setRefreshing(false);
				list.clear();
				inintHttp();
			}
		});
		BackAcitvity.back(back, MyTeamActivity.this);
	}
	public void inintView(){
		inflater = getLayoutInflater();
		addTeam = (ImageView) findViewById(R.id.addTeam);
		myTeamlListView = (ListView) findViewById(R.id.myTeamListView);
		back = (ImageView) findViewById(R.id.backImage);
		userHead = (ImageView) inflater.inflate(R.layout.myteamlist, null).findViewById(R.id.teamLeaderImage);
	}
	public void inintHttp(){
		//验证时否联网
		if(!CheckedNetWork.checkedConnection(this)){
			return;
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("curName", curUserName);
		map.put("action", "queryTeam");
		String result = null;
		fb = FinalBitmap.create(MyTeamActivity.this);
		try {
			result = HttpUtil.postRequest(url, map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			System.out.println("*******所有团队******"+result);
			JSONObject jsonObject = new JSONObject(result);
			JSONArray jsonArray1 = jsonObject.getJSONArray("teamListJson");
			
			list.clear();
			for(int i = 0;i<jsonArray1.length();i++){
				JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
				JSONArray jsonArray2 = jsonObject1.getJSONArray("teamListJson"+i);
				String team_name = jsonArray2.getJSONObject(0) .getString("team_name");
				String team_admin = jsonArray2.getJSONObject(1) .getString("team_admin");
				String admin_src = jsonArray2.getJSONObject(2) .getString("admin_src");
				Map<String, String> map2 = new HashMap<String, String>();
				map2.put("team", team_name);
				map2.put("teamLeaderName",team_admin);
				map2.put("url", admin_src);
				list.add(map2);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//刷新数据
		myTeamAdapter.notifyDataSetChanged();
	}
	/*********************************下一个监听事件结束后监听函数***********************************/
	@Override
	protected void onActivityResult(int REQUEST_CODE, int resultCode, Intent data) {
		inintHttp();
	}	
}
