package com.gem.taskplan.activity;

import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

public class TabHostActivity extends ActivityGroup{
	private TabHost tabHost;
	private Intent setiIntent;
	private Intent workNewsiIntent;
	private Intent workTableiIntent ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_tabhost);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		tabHost = (TabHost) super.findViewById(R.id.tabhost);
		tabHost.setup();
		tabHost.setup(this.getLocalActivityManager()); 
		initIntent();
		addSpec();
		tabHost.setCurrentTab(1);
		
		/*********************/
		Intent intent = getIntent();
		String action = intent.getStringExtra("action");
		if(action!=null&&action.equals("4")){
			tabHost.setCurrentTab(0);
			new AlertDialog.Builder(TabHostActivity.this).setTitle(intent.getStringExtra("message_send_user")+intent.getStringExtra("message_type"))
			.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			}).create().show();
		}
	}

	public void initIntent(){
		setiIntent = new Intent(TabHostActivity.this, SetActivity.class);
		workNewsiIntent = new Intent(TabHostActivity.this, WorkNewsActivity.class);
		workTableiIntent = new Intent(TabHostActivity.this, WorkTableActivity.class);
	}
	public void addSpec(){
		final View view1 = getLayoutInflater().inflate(R.layout.tab_host_footer1, null);
		final View view2 = getLayoutInflater().inflate(R.layout.tab_host_footer2, null);
		final View view3 = getLayoutInflater().inflate(R.layout.tab_host_footer3, null);
		//		TextView textView = (TextView) view1.findViewById(R.id.tab_text);
		//		ImageView imageView = (ImageView) view1.findViewById(R.id.tab_Image);
		tabHost.addTab(this.buildTagSpec("edit1",
				view1, workNewsiIntent));
		tabHost.addTab(this.buildTagSpec("edit2",
				view2, workTableiIntent));
		tabHost.addTab(this.buildTagSpec("edit3",
				view3, setiIntent));
		tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			
			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				if (tabId.equals("edit1")) {
				//	WorkNewsActivity.initList();刷新数据
					view2.setBackgroundResource(android.R.color.transparent);
					view3.setBackgroundResource(android.R.color.transparent);
					view1.setBackgroundResource(android.R.color.white);
//					view1.setBackgroundColor(0xFFFFFF);
				}else if (tabId.equals("edit2")) {
				//	WorkTableActivity.initList();刷新数据
					view1.setBackgroundResource(android.R.color.transparent);
					view3.setBackgroundResource(android.R.color.transparent);
					view2.setBackgroundResource(android.R.color.white);
				}else {
//					view1.setBackgroundColor(0xFFFFFF);
					view2.setBackgroundResource(android.R.color.transparent);
					view1.setBackgroundResource(android.R.color.transparent);
					view3.setBackgroundResource(android.R.color.white);
				}
//				Toast.makeText(TabHostActivity.this, tabId, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	/**
	 * 自定义创建标签项的方法
	 * @param tagName 标签标识
	 * @param tagLable 标签文字
	 * @param content 标签对应的内容
	 * @return
	 */
	private TabHost.TabSpec buildTagSpec(String tagName,View view,
			Intent content) {

		//    	return tabHost
		//    			.newTabSpec(tagName)
		//    			.setIndicator(tagLable,getResources().getDrawable(resource)).setContent(content);
		return tabHost
				.newTabSpec(tagName)
				.setIndicator(view).setContent(content);
	}
	
	@Override
	protected void onStart() {
		
		super.onStart();
	}
}

