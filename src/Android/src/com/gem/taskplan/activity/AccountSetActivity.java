package com.gem.taskplan.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

import javax.security.auth.PrivateCredentialPermission;

import com.gem.taskplan.adapter.AddCountAdapter;
import com.gem.taskplan.adapter.MemberListAdapter;
import com.gem.taskplan.service.BackAcitvity;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.service.MySQLiteOpen;
import com.gem.taskplan.service.ReceiveMessageService;
import com.gem.taskplan.service.SQLiteUtil;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AccountSetActivity extends Activity {

	private ImageView backImageView = null;
	private List<Map<String, String>> list;
	SharedPreferences sharedPreferences;
	AddCountAdapter adapter ;
	LinearLayout layout ;
	String[] nameData;
	MySQLiteOpen open;
	SQLiteUtil util;
	ListView lv;
	private String url;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_set);

		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);

		url ="/ProjectServlet";
		url = PropertiesUtil.getServerURL(this)+url;
		lv = (ListView)findViewById(R.id.lv);
		backImageView = (ImageView)findViewById(R.id.backImage);
		BackAcitvity.back(backImageView, AccountSetActivity.this);
		list = new ArrayList<Map<String, String>>();
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View view = layoutInflater.inflate(R.layout.addcount_foot,null);	
		layout = (LinearLayout) view.findViewById(R.id.addcount);
		lv.addFooterView(layout);
		init();
		adapter = new AddCountAdapter(list, this, R.layout.addcountlist);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intentserv = new Intent(AccountSetActivity.this, ReceiveMessageService.class);
				// 停止推送服务
				stopService(intentserv);
				TextView memberNameTextView = (TextView) view.findViewById(R.id.member);
				if(!(sharedPreferences.getString(memberNameTextView.getText().toString()+"2", "").length()==0)){
					Intent intent = new Intent(AccountSetActivity.this,TabHostActivity.class);
					Editor editor = sharedPreferences.edit();
					editor.putString("lastUser", memberNameTextView.getText().toString());
					editor.commit();
					startActivity(intent);
				}else{
					Intent intent = new Intent(AccountSetActivity.this,LoginActivity.class);
					intent.putExtra("name", memberNameTextView.getText().toString());
					startActivity(intent);
				}

				SysApplication.getInstance().exit();
				System.exit(0);
			}
		});
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int pos, long id) {
				final int position = pos;
				TextView memberNameTextView = (TextView) view.findViewById(R.id.member);
				final String name = memberNameTextView.getText().toString();
				new AlertDialog.Builder(AccountSetActivity.this).setTitle("确定移除"+name+"吗？")
				.setPositiveButton("确认", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						util =  new SQLiteUtil(open.getWritableDatabase());
						util.delete(name);
						list.remove(position);
						adapter.notifyDataSetChanged();
						//init();
					}
				}).setNegativeButton("取消", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}

				}).create().show();
				return false;
			}
		});

	}
	//初始化数据
	public void init(){
		
		if(!CheckedNetWork.checkedConnection(this)){
			return;
		}
		
		System.out.println("初始化账户信息！！！");
		list.clear();
		open = new MySQLiteOpen(this, "user.db", 2);
		util =  new SQLiteUtil(open.getWritableDatabase());
		nameData = util.query();
		sharedPreferences = getSharedPreferences("taskPlanShare", MODE_PRIVATE);
		String name = sharedPreferences.getString("lastUser", "");
		for(String aa :nameData){
			Map<String,String> map = new HashMap<String, String>();
			map.put("name", aa);
			if(name.equals(aa)){
				map.put("checked", "1");}else {
					map.put("checked", "0");
				}
			Map<String, String> map1 = new HashMap<String, String>();
			map1.put("userName", aa);
			map1.put("action", "getHead");

			String result = null;
			try {
				result = HttpUtil.postRequest(url, map1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(result.length()==0){
				map.put("url","");
			}else {
				map.put("url", result);
			}
			list.add(map);
		}
		for(Map<String, String> aa :list){
			System.out.println("数据库查询的数据"+aa.get("name"));
		}
		//adapter.notifyDataSetChanged();
	}
	
	public void addcount(View view){
		Intent intent = new Intent(AccountSetActivity.this,LoginActivity.class);
		startActivity(intent);
		SysApplication.getInstance().exit(AccountSetActivity.this);
		System.exit(0);
	}  
}

