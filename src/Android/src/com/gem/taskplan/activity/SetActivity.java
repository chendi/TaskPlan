package com.gem.taskplan.activity;

import com.gem.taskplan.service.DialogTool;
import com.gem.taskplan.service.ReceiveMessageService;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.provider.AlarmClock;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class SetActivity extends Activity{

	private String curUser;
	private ListView listView;
	private RelativeLayout remindSet = null;//提醒设置
	private RelativeLayout accountSet = null;//账户设置
	private RelativeLayout myMessage = null;//我的消息
	private RelativeLayout pushMessage = null;//消息推送

	private RelativeLayout userList = null;//成员列表
	private RelativeLayout myTeam = null;//我的团队

	private RelativeLayout versionUpgrade = null;//版本升级
	private RelativeLayout aboutTaskPlan = null;//关于taskPlan
	private RelativeLayout suggestions = null;//意见反馈

	private ImageView swith = null;
	private Button exitCurAccount = null;//退出当前账户
	private Intent remindIntent = null;
	private Intent memberIntent = null;
	private Intent accountSetIntent = null;
	private Intent myMessageIntent = null;
	private Intent sugesstioniIntent = null;
	private Intent myTeamSetIntent = null;
	private Intent versionUpgradeIntent = null;
	private LayoutInflater inflater = null;
	private View aboutViewDialog = null;
	private int[] id = {R.drawable.msp_switch_2x_on,R.drawable.msp_switch_2x_off};
	private SharedPreferences sharedPreferences;
	Editor editor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		
		sharedPreferences = SetActivity.this.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE);
		curUser = sharedPreferences.getString("lastUser", "");
		
		editor = sharedPreferences.edit();
		initView();
		initIntent();
		int i = sharedPreferences.getInt(curUser+"PushState", 0);
		swith.setImageResource(id[i]);
		
		exitCurAccount.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				new AlertDialog.Builder(SetActivity.this).setTitle("您确定您要退出当前账号吗？").setIcon(R.drawable.taskplan2)
				.setPositiveButton("退出", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
//						Intent intent = new Intent(SetActivity.this, ReceiveMessageService.class);
//						// 停止推送服务
//						stopService(intent);
						//跳转到登陆界面
						Intent intent2 = new Intent(SetActivity.this,LoginActivity.class);
						startActivity(intent2);
						//结束上一个账号的所有界面
						SysApplication.getInstance().exit(SetActivity.this);
						System.exit(0);
					}
				}).setNegativeButton("取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).create().show();
			}
		});
		
		suggestions.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(sugesstioniIntent);
			}
		});
		aboutTaskPlan.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inflater = getLayoutInflater();
				aboutViewDialog = inflater.inflate(R.layout.about_taskplan_textview, null);
				new AlertDialog.Builder(SetActivity.this).setView(aboutViewDialog).create().show();
			}
		});
		remindSet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//				startActivity(remindIntent);
				startActivity(new Intent(AlarmClock.ACTION_SET_ALARM)); 
			}
		});
		accountSet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(accountSetIntent);
			}
		});
		userList.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(memberIntent);
			}
		});
		myTeam.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(myTeamSetIntent);
			}
		});
		
		myMessage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(myMessageIntent);
			}
		});
		
		pushMessage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				System.out.println("------------------------"+id[1]);
				int temp = sharedPreferences.getInt(curUser+"PushState", 0);
				if (temp==1) {
					editor.putInt(curUser+"PushState", 0);
					temp = 0;
					// 启动推送服务
					Intent intent = new Intent(SetActivity.this, ReceiveMessageService.class);
					startService(intent);
				}else{
					editor.putInt(curUser+"PushState", 1);
					temp = 1;
					Intent intent = new Intent(SetActivity.this, ReceiveMessageService.class);
					// 停止推送服务
					stopService(intent);
				}
				editor.commit();
				swith.setImageResource(id[temp]);
			}
		});
		versionUpgrade.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(versionUpgradeIntent);
			}
		});
	}

	private void initIntent(){
		remindIntent = new Intent(SetActivity.this, RemindSetActivity.class);
		memberIntent = new Intent(SetActivity.this, MemberListActivity.class);
		accountSetIntent = new Intent(SetActivity.this, AccountSetActivity.class);
		myMessageIntent = new Intent(SetActivity.this,MessageActivity.class);
		myMessageIntent.putExtra("action","1");
		myTeamSetIntent = new Intent(SetActivity.this, MyTeamActivity.class);
		sugesstioniIntent = new Intent(SetActivity.this, SuggestionsActivity.class);
		versionUpgradeIntent = new Intent(SetActivity.this,VersionUpgradeActivity.class);
	}
	private void initView(){
		listView = (ListView) findViewById(R.id.listView);
		View header = getLayoutInflater().inflate(R.layout.set_scrollview, null);

		swith = (ImageView) header.findViewById(R.id.swith);
		remindSet =  (RelativeLayout) header.findViewById(R.id.remindSet);
		accountSet = (RelativeLayout) header.findViewById(R.id.accountSet);
		myMessage = (RelativeLayout) header.findViewById(R.id.myMessage);
		pushMessage = (RelativeLayout) header.findViewById(R.id.pushMessage);


		myTeam = (RelativeLayout) header.findViewById(R.id.myTeam);
		userList = (RelativeLayout) header.findViewById(R.id.userList);

		versionUpgrade = (RelativeLayout) header.findViewById(R.id.versionUpgrade);
		aboutTaskPlan = (RelativeLayout) header.findViewById(R.id.aboutTaskPlan);
		suggestions = (RelativeLayout) header.findViewById(R.id.suggestions);

		exitCurAccount = (Button) header.findViewById(R.id.exitCurAccount);

		listView.addHeaderView(header);
		listView.setAdapter(new ArrayAdapter(this, 1));
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){
			DialogTool.existDialog(this);
		}
		return super.onKeyDown(keyCode, event);
	}

}
