package com.gem.taskplan.activity;

import com.gem.taskplan.service.BackAcitvity;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ImageView;

public class RemindSetActivity extends Activity {

	private ImageView backImageView = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remind_set);
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		backImageView = (ImageView)findViewById(R.id.backImage);
		BackAcitvity.back(backImageView, RemindSetActivity.this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.remind_set, menu);
		return true;
	}

}
