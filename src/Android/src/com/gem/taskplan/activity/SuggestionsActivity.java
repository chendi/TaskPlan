package com.gem.taskplan.activity;

import com.gem.taskplan.service.BackAcitvity;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SuggestionsActivity extends Activity {

	private ImageView backAccount;
	private ImageView finishText;
//	private TextView finishText;
	private EditText suggestionsContent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_suggestions);
		
		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		
		backAccount = (ImageView) findViewById(R.id.backImage);
//		finishText = (TextView) findViewById(R.id.finishText);
		finishText = (ImageView) findViewById(R.id.finishText);
		suggestionsContent = (EditText) findViewById(R.id.suggestionsContent);
		
		finishText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(suggestionsContent.getText().toString().trim().length()==0){
					Toast.makeText(SuggestionsActivity.this,"您还没有输入您宝贵的建议", Toast.LENGTH_LONG).show();
					return;
				}
				Toast.makeText(SuggestionsActivity.this,"您的意见已提交，十分感谢您宝贵的建议", Toast.LENGTH_LONG).show();
				finish();
			}
		});
		BackAcitvity.back(backAccount, this);//返回监听
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.suggestions, menu);
		return true;
	}
}
