package com.gem.taskplan.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gem.taskplan.adapter.MessageListAdapter;
import com.gem.taskplan.javaBean.Message;
import com.gem.taskplan.service.MySQLiteOpen2;
import com.gem.taskplan.service.SQLiteUtil2;
import com.gem.taskplan.util.SysApplication;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class MessageActivity extends Activity {

	private String curUser;
	private MySQLiteOpen2 open;
	private SQLiteUtil2 util;
	private String message_time ;
	private String message_send_user ;
	private String message_content ;
	private String message_type ;
	private String message_state ;

	private LayoutInflater inflater;
	private View view;
	//	private Spinner spiState = null;
	//	private ArrayAdapter<CharSequence> adapterState = null;
	//	private List<CharSequence> dataState = null;
	private ListView messageListView;
	private MessageListAdapter messageListAdapter;
	private List<Map<String, String>> list = new ArrayList<Map<String,String>>();
	//private int[] message = {R.drawable.agree,R.drawable.cancel};
	private static int temp = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);

		/***********退出程序************/
		SysApplication.getInstance().addActivity(this);
		/*************控件*****************/
		this.open = new MySQLiteOpen2(this, "message.db", 4);	
		inflater = getLayoutInflater();
		view = inflater.inflate(R.layout.activity_message_itemtext, null);
		messageListView = (ListView) findViewById(R.id.message_ListView);
		curUser = getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");

		/**********适配器***********/
		messageListAdapter = new MessageListAdapter(list, MessageActivity.this);
		messageListView.setAdapter(messageListAdapter);
		
		//初始化消息列表的list集合数据源
		initMessages();

		Intent intent = getIntent();
		String actionString = intent.getStringExtra("action");
		//Toast.makeText(MessageActivity.this,"action = "+actionString,Toast.LENGTH_LONG).show();
		if(actionString.equals("1")){
			//初始化消息列表的list集合数据源
//			initMessages();
		}else if(actionString.equals("3")){
			new AlertDialog.Builder(MessageActivity.this).setTitle(intent.getStringExtra("message_send_user")+intent.getStringExtra("message_type"))
			.setPositiveButton("知道了", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			}).create().show();
		}else {
			message_time = intent.getStringExtra("message_time");
			message_send_user = intent.getStringExtra("message_send_user");
			message_content = intent.getStringExtra("message_content");
			message_type = intent.getStringExtra("message_type");
			message_state = intent.getStringExtra("message_state");

			util = new SQLiteUtil2(open.getWritableDatabase());
			Message message = new Message();
			message.setMessage_time(message_time);
			message.setMessage_send_user(message_send_user);
			message.setMessage_content(message_content);
			message.setMessage_receive_user(curUser);
			message.setMessage_type(message_type);
			message.setMessage_state("3");
			util.insert(message);

			Map<String, String> map = new HashMap<String, String>();
			map.put("message_time", message_time);
			map.put("message_send_user", message_send_user);
			if (message_type!=null&&message_type.equals("加您为好友")) {
				message_content = "附加信息："+message_content;
			}
			map.put("message_content", message_content);
			map.put("message_type", message_type);
			map.put("message_state", "3");
			list.add(0,map);
			
			messageListAdapter.notifyDataSetChanged();
		}


	}

	//初始化消息列表的list集合数据源
	private void initMessages(){
		util = new SQLiteUtil2(open.getWritableDatabase());
		List<Message> listMessage = util.query(1);
		list.clear();
		for(Message message:listMessage){
			Map<String,String> map = new HashMap<String, String>();
			map.put("message_time", message.getMessage_time());
			map.put("message_send_user", message.getMessage_send_user());

			if (message.getMessage_type()!=null&&message.getMessage_type().equals("加您为好友")) {
				message_content = "附加信息："+message.getMessage_content();
			}

			map.put("message_content", message_content);
			map.put("message_type", message.getMessage_type());
			map.put("message_state", message.getMessage_state());
//			System.out.println("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"+message.getMessage_state());
			list.add(map);
		}
		messageListAdapter.notifyDataSetChanged();
	}
}
