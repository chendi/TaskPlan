package com.gem.taskplan.adapter;

import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalBitmap;

import com.gem.taskplan.activity.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MyTeamAdapter extends BaseAdapter {

	private Map<String, String> map = null;
	private List<Map<String, String>> list = null;
	private Context context = null;
	private LayoutInflater inflater = null;
	private FinalBitmap fb;
	public MyTeamAdapter(List<Map<String, String>> list, Context context) {
		super();
		this.list = list;
		this.context = context;
		inflater = inflater.from(context);
		fb = FinalBitmap.create(context);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String url = list.get(position).get("url");
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.myteamlist, null);
		}
		ImageView teamLeaderImage = (ImageView) convertView.findViewById(R.id.teamLeaderImage);
		TextView teamName = (TextView) convertView.findViewById(R.id.teamName);
		TextView teamLeaderName = (TextView) convertView.findViewById(R.id.teamLeaderName);
		teamName.setText(list.get(position).get("team"));
		teamLeaderName.setText(list.get(position).get("teamLeaderName"));
		if (url.length()==0) {
			teamLeaderImage.setImageResource(R.drawable.touxiang);
		}else {
			fb.display(teamLeaderImage,list.get(position).get("url"));
		}

		return convertView;


	}
}
