package com.gem.taskplan.adapter;

import java.net.URL;
import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalBitmap;

import com.gem.taskplan.activity.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MemberListAdapter extends BaseAdapter{

//	private Map<String, String> map = null;
	private List<Map<String, String>> list = null;
	private Context context = null;
	private LayoutInflater inflater = null;
	private FinalBitmap fb;
	private int resource;
	public MemberListAdapter(List<Map<String, String>> list, Context context,int resource) {
		super();
		this.list = list;
		this.context = context;
		inflater = inflater.from(context);
		fb = FinalBitmap.create(context);
		this.resource = resource;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String url = list.get(position).get("url");
		
		if (resource==R.layout.member_list) {
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.member_list, null);
			}
			ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImage);
			TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberName);
			TextView memberSex = (TextView) convertView.findViewById(R.id.memberSex);
			TextView memberEmailTextView = (TextView) convertView.findViewById(R.id.memberEmail);
			memberNameTextView.setText(list.get(position).get("name"));
			memberSex.setText(list.get(position).get("sex"));
			memberEmailTextView.setText(list.get(position).get("email"));
			if (url.length()==0) {
				memberImageView.setImageResource(R.drawable.touxiang);
			}else {
				fb.display(memberImageView,list.get(position).get("url"));
			}
		}else if (resource==R.layout.choose_member_list) {
			System.out.println("进入choose_member_list");
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.choose_member_list, null);
			}
//			memberImage memberName choosed
			ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImage);
			ImageView choosed = (ImageView) convertView.findViewById(R.id.choosed1);
			TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberName);
			choosed.setImageResource(R.drawable.checked);
			if (list.get(position).get("checked").equals("0")) {
				choosed.setVisibility(View.INVISIBLE);
			}else {
				choosed.setVisibility(View.VISIBLE);
			}
			if (url.length()==0) {
				memberImageView.setImageResource(R.drawable.touxiang);
			}else {
				fb.display(memberImageView,list.get(position).get("url"));
			}
			memberNameTextView.setText(list.get(position).get("name"));
		}else if(resource==R.layout.choose_member_list1){
			System.out.println("进入choose_member_list1");
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.choose_member_list1, null);
			}
			ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImage);
			TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberName);
			if (url.length()==0) {
				memberImageView.setImageResource(R.drawable.touxiang);
			}else {
				fb.display(memberImageView,list.get(position).get("url"));
			}
			memberNameTextView.setText(list.get(position).get("name"));
		}else if(resource==R.layout.choose_member_list2){
			System.out.println("进入choose_member_list2");
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.choose_member_list2, null);
			}
			ImageView memberImageView = (ImageView) convertView.findViewById(R.id.memberImage);
			TextView memberNameTextView = (TextView) convertView.findViewById(R.id.memberName);
			if (url.length()==0) {
				memberImageView.setImageResource(R.drawable.touxiang);
			}else {
				fb.display(memberImageView,list.get(position).get("url"));
			}
			memberNameTextView.setText(list.get(position).get("name"));
		}
		else {
			Toast.makeText(context, "布局文件异常", Toast.LENGTH_SHORT).show();;
		}
		return convertView;
		
	}

}
