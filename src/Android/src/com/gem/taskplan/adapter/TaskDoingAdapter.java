package com.gem.taskplan.adapter;

import java.util.List;
import com.gem.taskplan.activity.R;
import com.gem.taskplan.javaBean.DoingBean;
import com.gem.taskplan.javaBean.ToDoBean;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class TaskDoingAdapter extends BaseAdapter{

	private int resource;
	private LayoutInflater inflater;
	private List<DoingBean> list;

	public TaskDoingAdapter(Context context,List<DoingBean> list,int resource){
		super();
		this.list = list;
		this.resource = resource;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DoingBean bean = list.get(position);//获取一个Item的数据源
		convertView = inflater.inflate(resource, null);
		ProgressBar bar = (ProgressBar) convertView.findViewById(R.id.pbar1);
		TextView content = (TextView) convertView.findViewById(R.id.task_content2);
		TextView member = (TextView) convertView.findViewById(R.id.task_member2);
		TextView task_progress = (TextView) convertView.findViewById(R.id.task_progress2);
		TextView task_message = (TextView) convertView.findViewById(R.id.task_message2);
		TextView task_attachment = (TextView) convertView.findViewById(R.id.task_attachment2);
		TextView endtime = (TextView) convertView.findViewById(R.id.end_time2);
		content.setText(bean.getContent());
		member.setText(bean.getMember());
		task_progress.setText(bean.getTask_progress());
		task_message.setText(bean.getTask_message());
		task_attachment.setText(bean.getTask_attachment());
		endtime.setText(bean.getEndtime());

		if(bean.getTask_progress()!=null){
			String progress[] = bean.getTask_progress().split("\\/");
			bar.setProgress(Integer.parseInt(progress[0]));
			if(Integer.parseInt(progress[1])!=0){
				bar.setMax(Integer.parseInt(progress[1]));
			}
		}
		return convertView;
	}
}
