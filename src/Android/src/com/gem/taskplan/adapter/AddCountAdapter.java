package com.gem.taskplan.adapter;

import java.net.URL;
import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalBitmap;

import com.gem.taskplan.activity.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddCountAdapter extends BaseAdapter{

//	private Map<String, String> map = null;
	private List<Map<String, String>> list = null;
	private Context context = null;
	private LayoutInflater inflater = null;
	private FinalBitmap fb;
	private int resource;
	public AddCountAdapter(List<Map<String, String>> list, Context context,int resource) {
		super();
		this.list = list;
		this.context = context;
		inflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
		this.resource = resource;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String url = list.get(position).get("url");
		
		
		
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.addcountlist, null);
			}
			ImageView memberImageView = (ImageView) convertView.findViewById(R.id.Image);
			ImageView choosed = (ImageView) convertView.findViewById(R.id.choose);
			TextView memberNameTextView = (TextView) convertView.findViewById(R.id.member);
			choosed.setImageResource(R.drawable.checked);
			if (list.get(position).get("checked").equals("0")) {
				choosed.setVisibility(View.INVISIBLE);
			}else {
				choosed.setVisibility(View.VISIBLE);
			}
			if (url.length()==0) {
				memberImageView.setImageResource(R.drawable.touxiang);
			}else {
				fb.display(memberImageView,list.get(position).get("url"));
			}
			memberNameTextView.setText(list.get(position).get("name"));
			System.out.println("适配器内的位置处的名字"+position+"名字"+list.get(position).get("name"));
		
		return convertView;
		
	}

}
