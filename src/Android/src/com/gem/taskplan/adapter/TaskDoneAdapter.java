package com.gem.taskplan.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.gem.taskplan.activity.Fragment3;
import com.gem.taskplan.activity.R;
import com.gem.taskplan.javaBean.DoneBean;
import com.gem.taskplan.javaBean.ToDoBean;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class TaskDoneAdapter extends BaseAdapter{

	String url = "/TaskServlet";
	private int resource;
	private Context context;
	private LayoutInflater inflater;
	private List<DoneBean> list;
	private String curUser;

	public TaskDoneAdapter(Context context,List<DoneBean> list,int resource){
		super();
		this.list = list;
		this.resource = resource;
		this.context = context;
		inflater = LayoutInflater.from(context);
		url = PropertiesUtil.getServerURL(context)+url;
		this.curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DoneBean bean = list.get(position);//获取一个Item的数据源
		convertView = inflater.inflate(resource, null);
		final TextView content = (TextView) convertView.findViewById(R.id.task_content3);
		final TextView member = (TextView) convertView.findViewById(R.id.task_member3);
		TextView endtime = (TextView) convertView.findViewById(R.id.finish_time);
//		final TextView zanNumber = (TextView) convertView.findViewById(R.id.zanNumber);
		final TextView zanNumber = (TextView) convertView.findViewById(R.id.zanNumber);
		LinearLayout zan = (LinearLayout) convertView.findViewById(R.id.zan);
		content.setText(bean.getContent());
		member.setText(bean.getMember());
		endtime.setText(bean.getEndtime());
		zanNumber.setText(String.valueOf(bean.getZanNumber()));
		//点赞同步服务器
		zan.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				String[] memberArray =member.getText().toString().intern().split("\\|");
				System.err.println("当前用户名：：：：：："+curUser);
				for (int i = 0; i < memberArray.length; i++) {
					System.err.println("迭代：：：：：：："+i+memberArray[i]);
					if(memberArray[i].trim().equals(curUser)){
						Toast.makeText(context, "您不能给您自己点赞！",Toast.LENGTH_SHORT).show();
						return;
					}
				}
				
				new AlertDialog.Builder(context).setTitle("你想给  "+content.getText().toString()+" 的完成者一个赞吗？")
				.setIcon(R.drawable.zan1).setPositiveButton("赞", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						
						/*************验证是否联网***********/
						if(!CheckedNetWork.checkedConnection(context)){
							return;
						}
						String result = null;
						Map<String, String> map = new HashMap<String, String>();
						map.put("curTaskName", content.getText().toString());
						map.put("curUser", curUser);
						map.put("action", "updataZan");
						try {
							result = HttpUtil.postRequest(url, map);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(result.equals("yes")){
							Toast.makeText(context, "点赞成功！", Toast.LENGTH_SHORT).show();
							String zanString = zanNumber.getText().toString();
							zanNumber.setText(String.valueOf(Integer.parseInt(zanString)+1));
						}else {
							Toast.makeText(context, "网络异常！", Toast.LENGTH_SHORT).show();
						}
					}
				}).setNegativeButton("否", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				}).create().show();
			}
		});
		return convertView;
	}
}
