package com.gem.taskplan.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gem.taskplan.activity.R;
import com.gem.taskplan.activity.WorkTableActivity;
import com.gem.taskplan.service.CheckedNetWork;
import com.gem.taskplan.service.MySQLiteOpen2;
import com.gem.taskplan.service.SQLiteUtil2;
import com.gem.taskplan.util.HttpUtil;
import com.gem.taskplan.util.PropertiesUtil;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MessageListAdapter extends BaseAdapter{
	private List<Map<String, String>> list = null;
	private Context context = null;
	private LayoutInflater inflater = null;
	private String curUser;
	private MySQLiteOpen2 open;
	private SQLiteUtil2 util;
	private String urlString = "/ReceiveMessageServlet";
	public MessageListAdapter(List<Map<String, String>> list, Context context) {
		super();
		this.list = list;
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		curUser = context.getSharedPreferences("taskPlanShare", Activity.MODE_PRIVATE).getString("lastUser", "");
		urlString = PropertiesUtil.getServerURL(context)+urlString;
		this.open = new MySQLiteOpen2(context, "message.db", 4);
	}
	@Override
	public int getCount() {
		return list.size();
	}
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int posi, View convertView, ViewGroup parent) {
		final int position = posi;
		final Map<String, String> map = list.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.activity_message_itemtext, null);
		}
//		final Button agreeButton = (Button) convertView.findViewById(R.id.agreeButton);
//		final Button refuseButton = (Button) convertView.findViewById(R.id.refuseButton);
		LinearLayout button_message = (LinearLayout) convertView.findViewById(R.id.button_message);
		button_message.removeAllViews();
		int count = button_message.getChildCount();
		System.out.println("MessageListAdapter界面,count="+count);
		System.out.println("MessageListAdapter界面"+map.get("message_time"));
		System.out.println("MessageListAdapter界面"+map.get("message_send_user"));
		System.out.println("MessageListAdapter界面"+map.get("message_content"));
		System.out.println("MessageListAdapter界面,message_state="+map.get("message_state"));

		if (map.get("message_send_user")!=null) {
			if(map.get("message_state").equals("3")){
				Button agreeButton = new Button(context);
				Button refuseButton = new Button(context);
				agreeButton.setText("同意");
				agreeButton.setTextSize(12);
				agreeButton.setBackgroundResource(R.drawable.button_back_ground);
				
				refuseButton.setText("拒绝");
				refuseButton.setTextSize(12);
				refuseButton.setBackgroundResource(R.drawable.button_back_ground);
				agreeButton.setVisibility(View.VISIBLE);
				refuseButton.setVisibility(View.VISIBLE);
				button_message.addView(agreeButton);
				button_message.addView(refuseButton);
				buttonClick(agreeButton, refuseButton, map,button_message);
			}else if(map.get("message_state").equals("2")){
				TextView textView = new TextView(context);
//				textView.setText("已同意");
//				textView.setTextColor(0xFF0000);
				textView.setHint("已同意");
				button_message.addView(textView);
			}else {
				TextView textView = new TextView(context);
//				textView.setText("已拒绝");
				textView.setHint("已同意");
//				textView.setTextColor(0xFF0000);
				button_message.addView(textView);
			}


		}
		TextView message_time = (TextView) convertView.findViewById(R.id.message_time1);
		TextView message_send_user = (TextView) convertView.findViewById(R.id.message_send_user);
		TextView message_content = (TextView) convertView.findViewById(R.id.message_content);
		//		TextView message_type = (TextView) convertView.findViewById(R.id.message_type);
		message_time.setText(map.get("message_time"));
		message_send_user.setText(map.get("message_send_user"));
		message_content.setText(map.get("message_content"));
		//		message_type.setText(map.get("message_type"));


		return convertView;
	}
	public void buttonClick(final Button agreeButton,final Button refuseButton,final Map<String, String> map,final LinearLayout button_message){
		agreeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				agreeButton.setText("已同意");
				agreeButton.setClickable(false);
				refuseButton.setClickable(false);
				Map<String, String> map1 = new HashMap<String, String>();
				map1.put("action1", "agree");
				map1.put("action", "addMemberRelation");
				map1.put("memberName",map.get("message_send_user"));
				map1.put("curName", curUser);
				/***********判断是否连接网络**********************/
				if(!CheckedNetWork.checkedConnection(context)){
					return;
				}
				
				String result = null;
				try {
					result = HttpUtil.postRequest(urlString, map1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (result==null) {
					Toast.makeText(context, "网络异常！", Toast.LENGTH_SHORT).show();
				}
				System.out.println("添加好友===="+result);
				if (result.equals("yes")) {
					button_message.removeAllViews();
					TextView textView = new TextView(context);
					textView.setHint("已同意");
					button_message.addView(textView);
					//open = new MySQLiteOpen2(context, "message.db", 1);		
					util = new SQLiteUtil2(open.getWritableDatabase());
					util.update(map.get("message_time"),map.get("message_send_user"),"2");
					Toast.makeText(context, "添加好友成功！", Toast.LENGTH_SHORT).show();

				}else {
					Toast.makeText(context, "添加好友失败！", Toast.LENGTH_SHORT).show();
				}


			}
		});
		refuseButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				refuseButton.setText("已拒绝");
				agreeButton.setClickable(false);
				refuseButton.setClickable(false);
				Map<String, String> map1 = new HashMap<String, String>();
				map1.put("action1", "refuse");
				map1.put("action", "addMemberRelation");
				map1.put("memberName",map.get("message_send_user"));
				map1.put("curName", curUser);
				/***********判断是否连接网络**********************/
				if(!CheckedNetWork.checkedConnection(context)){
					return;
				}
				
				String result = null;
				try {
					result = HttpUtil.postRequest(urlString, map1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (result.equals("yes")) {
					button_message.removeAllViews();
					TextView textView = new TextView(context);
					textView.setHint("已拒绝");
					button_message.addView(textView);
					System.out.println("MessageListAdapter界面，更改数据库");
					util = new SQLiteUtil2(open.getWritableDatabase());
					util.update(map.get("message_time"),map.get("message_send_user"),"1");
				}
			}
		});
	}
}
