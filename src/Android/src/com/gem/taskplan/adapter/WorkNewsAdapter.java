package com.gem.taskplan.adapter;

import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.gem.taskplan.activity.R;
import com.gem.taskplan.javaBean.WorkNewsBean;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WorkNewsAdapter extends BaseAdapter {
	private Context context;
	private List<WorkNewsBean> data ;
	private LayoutInflater layoutInflater;
	private FinalBitmap fb;
	public interface chatMMType{
		//别人发来的信息
		int MMcotype = 0;
		//自己发出去的信息
		int MMtotype = 1;
	}

	public WorkNewsAdapter(Context context, List<WorkNewsBean> data) {
		super();
		this.context = context;
		this.data = data;
		layoutInflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
	}

	@Override
	//获取listview的项数
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	//获取listview的项
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	//获取listview的id
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	//获取listview的项类型
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		WorkNewsBean entity = data.get(position);
		if(entity.isComMM()){
			return chatMMType.MMcotype;
		}else{
			return chatMMType.MMtotype;
		}
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		WorkNewsBean entity = data.get(position);
		String url = entity.getHead();
		boolean iscomMM = entity.isComMM();
		ViewHolder viewHolder = null;
		if(convertView == null){
			if(iscomMM){
				//如果是别人发来的消息，显示左边的气泡框
				convertView = layoutInflater.inflate(R.layout.left, null);
			}else{
				//自己发出去的消息，显示右边的气泡框
				convertView = layoutInflater.inflate(R.layout.right, null);
			}
			viewHolder = new ViewHolder();
			viewHolder.new_member = (TextView) convertView.findViewById(R.id.new_member);
			viewHolder.new_project = (TextView) convertView.findViewById(R.id.new_project);
			viewHolder.userhead = (ImageView) convertView.findViewById(R.id.userhead);
			viewHolder.new_time = (TextView) convertView.findViewById(R.id.new_time);
			viewHolder.iscomMM = iscomMM;
			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.new_member.setText(entity.getName());
		viewHolder.new_project.setText(entity.getText());
		viewHolder.new_time.setText(entity.getDate());
		if (url.length()==0) {
			viewHolder.userhead.setImageResource(R.drawable.touxiang);
		}else {
			fb.display(viewHolder.userhead,url);
		}
//		viewHolder.userhead.setImageURI(entity.getUri());
		return convertView;
	}

	static class ViewHolder{
		public TextView new_member;
		public ImageView userhead;
		public TextView new_project;
		public TextView new_time;
		boolean iscomMM;
	}
}
