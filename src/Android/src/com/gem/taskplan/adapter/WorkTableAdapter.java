/*package com.gem.taskplan.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.gem.taskplan.activity.R;
import com.gem.taskplan.service.DialogTool;
import com.gem.taskplan.util.HttpUtil;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class WorkTableAdapter extends BaseAdapter {

	private Context context;
	private List<Map<String, String>> list;
	private Holder holder;
	private View view;
	private LayoutInflater mInflater;
	
	public WorkTableAdapter(Context context, List<Map<String, String>> list) {
		super();
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}
	@Override
	public Object getItem(int position) {
		return position;
	}
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Map<String, String> map = new HashMap<String, String>();
		map = list.get(position);
		holder = new Holder();
		
		if (convertView != null) {
			holder = (Holder) convertView.getTag();
		} else {
			convertView = mInflater.inflate(R.layout.work_list_main, null);
			holder.name = (TextView) convertView.findViewById(R.id.workName_tv);
			holder.name.setText(map.get("name"));
			holder.out_Time = (TextView) convertView.findViewById(R.id.outOfTime_tv);
			holder.out_Time.setText(map.get("out_Time"));
			holder.creator = (TextView) convertView.findViewById(R.id.creator);
			holder.creator.setText(map.get("creator"));
			holder.editImage.setImageResource(R.drawable.edit);
			holder.name = (TextView) convertView.findViewById(R.id.workName_tv);
			holder.out_Time = (TextView) convertView.findViewById(R.id.outOfTime_tv);
			holder.creator = (TextView) convertView.findViewById(R.id.creator);
			
			convertView.setTag(holder);
		}
		holder.editImage.setOnClickListener(new lvButtonListener(convertView));
		return convertView;
	}

	private class Holder {
		TextView name;
		TextView out_Time;
		TextView creator;
		ImageView editImage;
	}
	
	class lvButtonListener implements View.OnClickListener {
		private View v1;
		lvButtonListener(View v) {
			this.v1 = v;
		}
		@Override
		public void onClick(View v) {
			view = mInflater.inflate(R.layout.addwork_dialog, null);
			Dialog dialog = new Dialog(context);
			DialogTool dialogTool = new DialogTool(context);
			dialogTool.getDialog(view, "修改项目").show();
			TextView textView2 = (TextView) view.findViewById(R.id.outOfTime_et);
			EditText editText3 = (EditText) view.findViewById(R.id.objectContent_et);
			Holder holder = (Holder) v1.getTag();
			  
			textView2.setText(holder.out_Time.getText().toString());
			Map< String , String> map = new HashMap<String, String>();
			map.put("action", "getProject_note");
			map.put("project_name", holder.name.getText().toString());
			String objectContent_et = httpSend(map);//获取当前条目的备注
			
			editText3.setText(objectContent_et);
		}
	}
	public String httpSend(Map<String, String> map){
		String result = null;
		try {
			result = HttpUtil.postRequest("http://10.204.1.1:8080/taskPlan/ProjectServlet", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
		
	}
	
	

}
*/